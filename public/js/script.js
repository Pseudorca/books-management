
var bookDataFromLocalStorage = [];

$(function () {
    loadBookData();

    // Window
    $("#insertWindow").kendoWindow({
        width: "330px",
        title: "新增書籍",
        modal: true,
        visible: false,
        actions: [
            "Minimize",
            "Close"
        ],
        close: insertWindowClose
    });
    // DropDownList for Window
    var catData = [
        { text: "資料庫", value: "database" },
        { text: "網際網路", value: "internet" },
        { text: "應用系統整合", value: "system" },
        { text: "家庭保健", value: "home" },
        { text: "語言", value: "language" }
    ]
    $("#book_category").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: catData,
        index: 0,
        change: switchImage
    });
    // DatePicker
    $("#bought_datepicker").kendoDatePicker({
        format: "yyyy-MM-dd",
        value: new Date(),
        dateInput: true
    });
    // Validator
    // https://docs.telerik.com/kendo-ui/api/javascript/ui/validator/configuration/rules
    $("#insertWindow").kendoValidator({
        rules: {
            textRule: function (input) {
                return $.trim(input.val()) !== "";
            },
            dateRule: function (input) {
                if (!input.is("[name=date]")) {
                    return true;
                }
                let date = kendo.parseDate(input.val());
                if (!date || date > new Date()) {
                    return false;
                }
                return true;
            }
        },
        messages: {
            textRule: "input space",
            dateRule: "select a invalid or future date"
        }
    });
    // Grid
    $("#book_grid").kendoGrid({
        dataSource: {
            data: bookDataFromLocalStorage,
            schema: {
                model: {
                    fields: {
                        BookId: { type: "int" },
                        BookName: { type: "string" },
                        BookCategory: { type: "string" },
                        BookAuthor: { type: "string" },
                        BookBoughtDate: { type: "string" },
                        BookPublisher: { type: "string" }
                    }
                }
            },
            pageSize: 20,
        },
        toolbar: kendo.template(`<div class='book-grid-toolbar'>
                                    <input id='book_search' class='book-grid-search' placeholder='我想要找......' type='text'></input>
                                    <select id='search_category'></select>
                                </div>`),
        height: 550,
        sortable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [
            { field: "BookId", title: "書籍編號", width: "10%" },
            { field: "BookName", title: "書籍名稱", width: "40%" },
            { field: "BookCategory", title: "書籍種類", width: "10%" },
            { field: "BookAuthor", title: "作者", width: "10%" },
            { field: "BookBoughtDate", title: "購買日期", width: "15%" },
            { field: "BookPublisher", title: "出版商", width: "15%" },
            { command: { text: "刪除", click: deleteBook }, title: " ", width: "120px" }
        ]

    });
    // DropDownList for Grid
    var fieldData = [
        { text: "書籍編號", value: "BookId" },
        { text: "書籍名稱", value: "BookName" },
        { text: "書籍種類", value: "BookCategory" },
        { text: "作者", value: "BookAuthor" },
        { text: "購買日期", value: "BookBoughtDate" },
        { text: "出版商", value: "BookPublisher" }
    ];
    $("#search_category").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: fieldData,
        index: 0
    });

    // bind action
    $("#insertWindowBtn").on('click', function () {
        $("#insertWindow").data("kendoWindow").open().center();
        $(this).fadeOut();
    });
    $("#addBook").on('click', insertBook);
    $("#book_search").on('input', searchBook);
})

function saveBookData() {
    localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
}

function loadBookData() {
    bookDataFromLocalStorage = JSON.parse(localStorage.getItem("bookData"));
    if (bookDataFromLocalStorage == null) {
        bookDataFromLocalStorage = bookData;
        saveBookData();
    }
}

function switchImage() {
    let filename = $("#book_category").data("kendoDropDownList").value();
    $(".book-image").attr('src', `image/${filename}.jpg`);
}

/**
 * close add book window and initialize the value
 * https://docs.telerik.com/kendo-ui/api/javascript/ui/dropdownlist/methods/select
 */
function insertWindowClose() {
    let dropDownList = $("#book_category").data("kendoDropDownList");
    dropDownList.select(0);
    dropDownList.trigger("change");

    $("#book_name").val("");
    $("#book_author").val("");
    $("#book_publisher").val("");
    $("#bought_datepicker").data("kendoDatePicker").value(new Date());
    $("#insertWindow").data("kendoValidator").reset();

    $("#insertWindowBtn").fadeIn();
}

/**
 * insert book to dataSource and localStorage
 * https://docs.telerik.com/kendo-ui/globalization/intl/dateformatting
 */
function insertBook() {
    if (!$("#insertWindow").data("kendoValidator").validate()) {
        return;
    }

    let maxId = 0;
    let gridData = $("#book_grid").data("kendoGrid").dataSource.data();
    for (let i = 0; i < gridData.length; ++i) {
        if (maxId < gridData[i].BookId) {
            maxId = gridData[i].BookId;
        }
    }
    let obj = {
        "BookId": (maxId + 1),
        "BookName": $("#book_name").val(),
        "BookCategory": $("#book_category").data("kendoDropDownList").text(),
        "BookAuthor": $("#book_author").val(),
        "BookBoughtDate": kendo.toString($("#bought_datepicker").data("kendoDatePicker").value(), "yyyy-MM-dd"),
        "BookPublisher": $("#book_publisher").val()
    };
    $("#book_grid").data("kendoGrid").dataSource.add(obj);
    bookDataFromLocalStorage.push(obj);
    saveBookData();

    $("#insertWindow").data("kendoWindow").close();
}

/**
 * remove selected row from dataSource and localStorage
 * https://docs.telerik.com/kendo-ui/api/javascript/kendo/methods/confirm
 * https://docs.telerik.com/kendo-ui/api/javascript/data/datasource/methods/getbyuid
 * https://docs.telerik.com/kendo-ui/api/javascript/ui/grid/methods/removerow
 */
function deleteBook(e) {
    let row = $(e.target).closest("tr");
    let grid = $("#book_grid").data("kendoGrid");

    let uid = row.data('uid');
    let data = grid.dataSource.getByUid(uid);
    kendo.confirm(`確定刪除 《${data.BookName}》?`)
        .done(function () {
            for (let i = 0; i < bookDataFromLocalStorage.length; ++i) {
                if (bookDataFromLocalStorage[i].BookId == data.BookId) {
                    grid.removeRow(row);
                    bookDataFromLocalStorage.splice(i, 1);
                    saveBookData();
                }
            }
        });
}

/**
 * filter data in grid by input event
 * https://docs.telerik.com/kendo-ui/api/javascript/data/datasource/configuration/filter
 */
function searchBook(e) {
    $("#book_grid").data("kendoGrid").dataSource.filter({
        field: $("#search_category").data("kendoDropDownList").value(),
        operator: "contains",
        value: $(e.target).val()
    });
}
