# Books Management System

## Progress
- [x] image switching
- [x] change kendo card section to kendo window
- [x] remove data from local storage
- [x] insert data to local storage
- [x] add kendo validate to insert function
- [x] add search function
